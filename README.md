# PieChart
The PieChart is a QControl and requires the QControl Toolkit from the [LabVIEW Tools Network](http://bit.ly/QControlsLVTool).  It inherits from and extends the Picture control.  It implements the a Pie Chart with Title, Legend, and selectable pie slices.
